package simuPremium.models.services.api;

import simuPremium.models.commons.GenericServiceApi;
import simuPremium.models.entity.Empresa;

public interface EmpresaService extends GenericServiceApi<Empresa,Long>
{

}
