package simuPremium.models.services.api;

import simuPremium.models.commons.GenericServiceApi;
import simuPremium.models.entity.Estudiante;

public interface EstudianteService extends GenericServiceApi<Estudiante,Long>
{
}
