package simuPremium.models.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import simuPremium.models.commons.GenericServiceImpl;
import simuPremium.models.dao.EstudianteDao;
import simuPremium.models.entity.Estudiante;
import simuPremium.models.services.api.EstudianteService;

import java.io.Serializable;

@Service("EstudianteService")
public class EstudianteServiceImpl extends GenericServiceImpl<Estudiante,Long> implements EstudianteService
{

    @Autowired
    private EstudianteDao dao;

    @Override
    public CrudRepository<Estudiante, Long> getDao()
    {
        return dao;
    }
}
