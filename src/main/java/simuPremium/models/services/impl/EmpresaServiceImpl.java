package simuPremium.models.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import simuPremium.models.dao.EmpresaDao;
import simuPremium.models.commons.GenericServiceImpl;
import simuPremium.models.entity.Empresa;
import simuPremium.models.services.api.EmpresaService;

@Service("EmpresaService")
public class EmpresaServiceImpl extends GenericServiceImpl<Empresa,Long> implements EmpresaService
{
    @Autowired
    private EmpresaDao dao;

    @Override
    public CrudRepository<Empresa, Long> getDao()
    {
        return dao;
    }
}
