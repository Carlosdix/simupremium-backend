package simuPremium.models.dao;

import org.springframework.data.repository.CrudRepository;
import simuPremium.models.entity.Estudiante;

public interface EstudianteDao extends CrudRepository<Estudiante,Long>
{
}
