package simuPremium.models.dao;

import org.springframework.data.repository.CrudRepository;
import simuPremium.models.entity.Empresa;

public interface EmpresaDao extends CrudRepository<Empresa,Long>
{
}
