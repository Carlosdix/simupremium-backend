package simuPremium.models.entity;

import javax.persistence.*;

@Entity
@Table(name = "empresas")
public class Empresa
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long nit;

    private String nombre;

    private String pais;

    private String departamento;

    private String ciudad;

    @Column(name = "codigo_postal")
    private String codigoPostal;

    @Column(name = "correo")
    private String correoElectronico;

    private String telefono;

    @Column(name = "pagina_web")
    private String paginaWeb;

    public Empresa() {
    }

    public Empresa(Long nit, String nombre, String pais, String departamento, String ciudad,
                   String codigoPostal, String correoElectronico, String telefono, String paginaWeb) {
        this.nit = nit;
        this.nombre = nombre;
        this.pais = pais;
        this.departamento = departamento;
        this.ciudad = ciudad;
        this.codigoPostal = codigoPostal;
        this.correoElectronico = correoElectronico;
        this.telefono = telefono;
        this.paginaWeb = paginaWeb;
    }

    public Long getNit() {
        return nit;
    }

    public void setNit(Long nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public void setPaginaWeb(String paginaWeb) {
        this.paginaWeb = paginaWeb;
    }
}
