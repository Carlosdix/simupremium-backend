package simuPremium.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import simuPremium.models.entity.Estudiante;
import simuPremium.models.services.api.EstudianteService;

import java.util.List;

@CrossOrigin(origins = {"*","http://localhost:4200"})
@RestController
@RequestMapping("/estudiantes")
public class EstudianteRestController
{
    @Autowired
    @Qualifier("EstudianteService")
    private EstudianteService service;

    @GetMapping("/all")
    public List<Estudiante> getAll()
    {
        return service.getAll();
    }

    @GetMapping("/{documento}")
    public Estudiante findByID(@PathVariable Long documento)
    {
        return service.findByID(documento);
    }

    @PostMapping("/all")
    public ResponseEntity<Estudiante> save(@RequestBody Estudiante pEstudiante)
    {
        final Estudiante estudiante = service.save(pEstudiante);
        return new ResponseEntity<Estudiante>(estudiante, HttpStatus.CREATED);
    }

    @PutMapping("/{documento}")
    @ResponseStatus(HttpStatus.CREATED)
    public Estudiante update( @RequestBody Estudiante pEstudiante,@PathVariable Long documento )
    {
        final Estudiante estudiante = service.findByID(documento);
        estudiante.setNombres( pEstudiante.getNombres() );
        estudiante.setApellidos( pEstudiante.getApellidos() );
        estudiante.setCorreo( pEstudiante.getCorreo() );
        estudiante.setCelular( pEstudiante.getCelular() );
        estudiante.setDireccion( pEstudiante.getDireccion() );
        return service.save(estudiante);
    }
    @DeleteMapping("/{documento}")
    public ResponseEntity<Estudiante>delete(@PathVariable Long documento)
    {
        final Estudiante estudiante = service.findByID(documento);
        if (estudiante!=null)
            service.delete(documento);
        else
            return new ResponseEntity<Estudiante>(estudiante,HttpStatus.NO_CONTENT);
        return new ResponseEntity<Estudiante>(estudiante,HttpStatus.OK);

    }
}
