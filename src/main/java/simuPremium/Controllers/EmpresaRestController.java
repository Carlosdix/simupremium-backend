package simuPremium.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import simuPremium.models.entity.Empresa;
import simuPremium.models.services.api.EmpresaService;

import java.util.List;

@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/empresas")
public class EmpresaRestController
{
    @Autowired
    @Qualifier("EmpresaService")
    private EmpresaService service;

    @GetMapping("/all")
    public List<Empresa> getAll()
    {
        return service.getAll();
    }

    @GetMapping("/{nit}")
    public Empresa findByID(@PathVariable Long nit)
    {
        return service.findByID(nit);
    }

    @PostMapping("/all")
    public ResponseEntity<Empresa> save(@RequestBody Empresa pEmpresa)
    {
        final Empresa empresa = service.save(pEmpresa);
        return new ResponseEntity<Empresa>(empresa, HttpStatus.CREATED);
    }

    @PutMapping("/{nit}")
    @ResponseStatus(HttpStatus.CREATED)
    public Empresa update( @RequestBody Empresa pEmpresa,@PathVariable Long nit )
    {
        final Empresa empresa = service.findByID( nit );
        empresa.setNombre( pEmpresa.getNombre() );
        empresa.setPais( pEmpresa.getPais() );
        empresa.setDepartamento( pEmpresa.getDepartamento() );
        empresa.setCiudad( pEmpresa.getCiudad() );
        empresa.setCodigoPostal( pEmpresa.getCodigoPostal() );
        empresa.setCorreoElectronico( pEmpresa.getCorreoElectronico() );
        empresa.setTelefono( pEmpresa.getTelefono() );
        empresa.setPaginaWeb( pEmpresa.getPaginaWeb() );
        return service.save(empresa);
    }

    @DeleteMapping("/{nit}")
    public ResponseEntity<Empresa> delete(@PathVariable Long nit)
    {
        final Empresa empresa = service.findByID(nit);
        if (empresa != null)
            service.delete(nit);
        else
            return new ResponseEntity<Empresa>(empresa,HttpStatus.NO_CONTENT);
        return new ResponseEntity<Empresa>(empresa,HttpStatus.OK);
    }
}
